#!/bin/bash

#-------------------------------------------------------------------------------
#- Name:        : deployImageStorage.sh
#- Alias:       : -
#- Author:      : Dean Terneu
#- Commit:      : Deploy image uploader backend and frontend applications on Gcloud Run
#- Required     : Gcloud account and Gcloud commands available + Docker available
#- Description: : INFRA3 - Final Project
#- Usage:       : ./deployImageStorage
#-------------------------------------------------------------------------------

# -h or --help will provide the user with script execution info
if [[ "$1" = "-h" || "$1" = "--help" ]];

then
        echo "---------------------------------------------------------------"
        echo "#########################################"
        echo "####        Help Instructions        ####"
        echo "#########################################"
        echo ""
        echo "Welcome to deploying you're own Image Storage application via Cloud Run "
        echo
        echo "#########################################"
        echo "####          Prerequisites          ####"
        echo "#########################################"
        echo
        echo "1) Have an active Gcloud account with at least one project"
        echo "2) Have the gcloud commands available"
        echo "3) Have Docker commands available"
        echo
        echo "#########################################"
        echo "####             Breakdown           ####"
        echo "#########################################"
        echo
        echo "1) Select your Gcloud project"
        echo "2) Select or create a new google registry"
        echo "3) Both frontend and backend images will be pulled from Docker"
        echo "4) Both frontend and backend images will be pushed to your Google Registry"
        echo "5) Both frontend and backend applications will be deployed on Gcloud Run"
        echo "6) After deployment you will get your url link and start usung your Image Storage "
        echo "---------------------------------------------------------------"
fi

# Check if gcloud is installed
if ! command -v gcloud &> /dev/null; then
    stop_moving_dots $!
    echo "Error: Google Cloud SDK (gcloud) is not installed."
    echo "This deployment requires Google Cloud Platform (GCP) and gcloud to be installed."
    echo "Please install the Google Cloud SDK and create an account: https://cloud.google.com/sdk/"
    exit 1
fi

# Check if Docker is installed
if ! command -v docker &> /dev/null; then
    echo "Error: Docker is not installed. Please install Docker first."
    exit 1
fi
echo "Docker installed..."

# Check if Docker daemon is running
if ! docker info &> /dev/null; then
    stop_moving_dots $!
    echo "Docker daemon is not running. Attempting to start Docker daemon..."
    if ! sudo systemctl start docker; then
        echo "Error: Failed to start Docker daemon. Please start the Docker daemon manually."
        exit 1
    fi
    exit 1
fi
echo "Docker daemon started successfully..."

# Authenticate with Google Cloud Artifact Registry
if ! gcloud auth configure-docker europe-west1-docker.pkg.dev -q; then
    stop_moving_dots $!
    echo "Error: Failed to authenticate Docker with Google Cloud Artifact Registry."
    exit 1
fi
echo "Docker succesfully authenticated with Google Cloud Artifact Registry."

# Stop moving dots and print title again
stop_moving_dots $!
echo -e "\nDone!"

echo ""
echo "#########################################"
echo "####          Google Project         ####"
echo "#########################################"
echo ""

# Prompt user to choose Google Cloud project
echo "Please choose your Google Cloud project:"
gcloud projects list
read -p "Enter your project ID: " project_id

# Validate if the chosen project exists
project_exists=$(gcloud projects list --filter="PROJECT_ID=$project_id" --format="value(PROJECT_ID)")
if [[ -z $project_exists ]]; then
    echo "Error: Project '$project_id' does not exist or you do not have permissions to access it."
    exit 1
fi

echo ""
echo "#########################################"
echo "####    Google Artifact Repository   ####"
echo "#########################################"
echo ""

# Prompt user for option
echo "Would you like to push this project to an existing Google Cloud Artifact Repository?"
echo "1. Yes, push to an existing repository."
echo "2. No, create a new repository."
read -p "Enter your choice (1 or 2): " choice
#fuction to push to an existing repository
function push_to_existing_repo() {
    local project_id=$1
    local repo_list=$(gcloud artifacts repositories list --project=$project_id --format="value(NAME)")

    echo "Existing repositories in project '$project_id':"
    echo "$repo_list"

    echo ""
    read -p "Enter the name of the existing repository to push to (copy the fill line from projects/... ): " existing_repo

    # Validate if the chosen repository exists
    if [[ ! $repo_list =~ (^|[[:space:]])"$existing_repo"($|[[:space:]]) ]]; then
        echo "Error: Repository '$existing_repo' does not exist in project '$project_id'."
        exit 1
    fi

    echo "$existing_repo"
    existing_repo_name_only=$(echo "$existing_repo" | awk -F '/' '{print $NF}')
    echo "$existing_repo_name_only"
    echo "You chose to push to repository '$existing_repo'."
}



# Function to create a new repository
function create_new_repo() {
    local project_id=$1

    echo "Creating a new Artifact Repository in project '$project_id'..."

    local valid_name_regex='^[a-z][a-z0-9-]*[a-z0-9]$'

    while true; do
        read -p "Enter the name of the new repository: " new_repo

        #Set global variable
        existing_repo_name_only=$new_repo

        if [[ ! $new_repo =~ $valid_name_regex ]]; then
            echo "Error: Repository name may only contain lowercase letters, numbers, and hyphens, and must begin with a letter and end with a letter or number."
        else
            break
        fi
    done

    gcloud artifacts repositories create $new_repo --location=europe-west1 --repository-format=docker --project=$project_id

    echo "New repository '$new_repo' created successfully."
}

#validate user input
if [[ $choice == "1" ]]; then
    # Push to existing repository
    push_to_existing_repo $project_id
elif [[ $choice == "2" ]]; then
    # Create a new repository
    echo "You chose to create a new repository."
    create_new_repo $project_id
else
    # Invalid choice
    echo "Invalid choice. Please enter 1 or 2."
fi

# Pull images from DockerHub
echo ""
echo "#########################################"
echo "####    Pull images from DockerHub   ####"
echo "#########################################"
echo ""

# Pull backend image from DockerHub

docker pull deanterneu/backend-cloudrun-amd64

if [ $? -eq 0 ]; then
    echo "Backend image pulled successfully from DockerHub."
else
    echo "Error: Failed to pull backend image from DockerHub."
    exit 1
fi
echo ""

# Pull frontend image from DockerHub

docker pull deanterneu/frontend-cloudrun-amd64

if [ $? -eq 0 ]; then
    echo "Frontend image successfully from DockerHub."
else
    echo "Error: Failed to pull frontend image from DockerHub."
    exit 1
fi
echo ""

# Tag and push images to Google Artifact Registry
echo "#########################################"
echo "####          Push To Registry       ####"
echo "#########################################"
echo ""

#echo "Authenticating with Google Cloud Artifact Registry..."
#gcloud auth configure-docker europe-west1-docker.pkg.dev -q
#echo "Docker is now authenticated with Google Cloud Artifact Registry."

# Tag and push backend application
echo "Tagging and pushing backend image to Google Artifact Registry..."
docker tag deanterneu/backend-cloudrun-amd64 europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/backend
docker push europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/backend

#echo "Backend image pushed successfully."

# Tag and push frontend  application
echo "Tagging and pushing frontend image to Google Artifact Registry..."
docker tag deanterneu/frontend-cloudrun-amd64 europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/frontend
docker push europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/frontend
echo "Frontend image pushed successfully."

echo ""
echo "#########################################"
echo "####     Deployment - Gcloud Run     ####"
echo "#########################################"
echo ""

# Deploy backend application
echo "Deploying backend application"
deployment_backend_output=$(gcloud run deploy backend --project $project_id --image europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/backend --region=europe-west1 --allow-unauthenticated --port=8086 2>&1)
if [ $? -eq 0 ]; then

        # Extract service URL using regex
        service_url=$(echo "$deployment_backend_output" | grep -o 'https://[^ ]*')

        if [ -n "$service_url" ]; then
                echo "Service URL: $service_url"
		echo "gcloud run deploy frontend --project $project_id --image europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/frontend --region=europe-west1 --allow-unauthenticated --set-env-vars='REACT_APP_BACKEND_URL=${service_url}' 2>&1"

        else
        echo "Error: Failed to extract service URL."
        fi
else
        echo "Error: backend deployment failed."
fi

# Deploy frontend service

echo ""
echo "Deploying frontend application..."

deployment_frontend_output=$(gcloud run deploy frontend --project $project_id --image europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/frontend --region=europe-west1 --allow-unauthenticated --set-env-vars="REACT_APP_BACKEND_URL=https://backend-qeonvskqnq-ew.a.run.app" 2>&1)

#deployment_frontend_output=$(gcloud run deploy frontend \
#    --project $project_id \
#    --image europe-west1-docker.pkg.dev/$project_id/$existing_repo_name_only/frontend \
#    --region=europe-west1 \
#    --allow-unauthenticated \
#    --set-env-vars=REACT_APP_BACKEND_URL=${service_url} \
#    2>&1)

# Check the exit status of the frontend deployment command
if [ $? -eq 0 ]; then
    # Extract frontend service URL using regex
    frontend_service_url=$(echo "$deployment_frontend_output" | grep -o 'https://[^ ]*')

    if [ -n "$frontend_service_url" ]; then
    echo ""
    echo "#########################################"
    echo "####             SUCCESS!            ####"
    echo "#########################################"
    echo ""

    echo "Start uploading your images -> $frontend_service_url"
    else
        echo "Error: Failed to extract frontend service URL."
    fi
else
    echo "Error: Frontend deployment failed."
fi


