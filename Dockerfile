FROM gradle:jdk17
LABEL author="Dean Terneu"
WORKDIR /app

COPY build.gradle.kts .
COPY settings.gradle.kts .
COPY gradlew .
COPY gradle/ ./gradle/

# Copy the source code
COPY src/ ./src/

# Build the Spring Boot application
RUN ./gradlew build

# Expose the port the app runs on
EXPOSE 8086

# Specify the command to run your Spring Boot application
CMD ["java", "-jar", "build/libs/backend-0.0.1-SNAPSHOT.jar"]
#asdfaf