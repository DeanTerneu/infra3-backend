# INFRA 3 - final project

## server - back-end application

## AUTHOR DETAILS

Dean Terneu - ACS202
dean.terneu@student.kdg.be
student-number: 0163905-72

## APPLICATION

This is a serverside application for handling API request.
you can:

* upload
* read 
* delete 

images from a gcloud bucket.

## PORTS

- test locally on port 8086
- you can test with the front-end application running on 8080
    * [backend](https://gitlab.com/DeanTerneu/infra3-frontend)