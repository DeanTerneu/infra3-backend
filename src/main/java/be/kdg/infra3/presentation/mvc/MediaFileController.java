package be.kdg.infra3.presentation.mvc;

import be.kdg.infra3.configuration.MyStorageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("/mediafile")
public class MediaFileController {


    private final MyStorageService myStorageService;

    public MediaFileController(MyStorageService myStorageService) {
        this.myStorageService = myStorageService;
    }

    @GetMapping
    public String initialPageLoad(){

//        try {
//            myStorageService.validateCredentials();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        return "mediaFile";
    }

}
