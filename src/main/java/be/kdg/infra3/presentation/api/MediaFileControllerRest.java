package be.kdg.infra3.presentation.api;

import be.kdg.infra3.configuration.MyStorageService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/mediafiles")
public class MediaFileControllerRest {

    private final MyStorageService storageService;

    private static final String BUCKET_NAME = "testbucketttt123";

    public MediaFileControllerRest(MyStorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("")
    public ResponseEntity<List<URL>> getImageUrls() {
        try {
            List<URL> imageUrls = storageService.getImageUrls(BUCKET_NAME);
            return ResponseEntity.ok(imageUrls);
        } catch (IOException e) {
            e.printStackTrace();
            // Handle exception
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
        try {
            // Generate a unique filename
            String filename = file.getOriginalFilename();

            // Upload the file to Google Cloud Storage
            String imageUrl = storageService.uploadFileToBucket(BUCKET_NAME, filename, file.getBytes());

            return ResponseEntity.ok(imageUrl);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload image");
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteImage(@RequestBody Map<String, String> requestBody) {
        try {
            String imageUrl = requestBody.get("imageUrl");
            // Parse the imageUrl and extract the filename or unique identifier
            // Call the delete method in your storage service passing the filename or unique identifier
            storageService.deleteFileFromBucket(BUCKET_NAME, imageUrl);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
