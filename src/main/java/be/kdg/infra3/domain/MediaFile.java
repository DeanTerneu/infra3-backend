package be.kdg.infra3.domain;

public class MediaFile {

    private String fileName;

    public MediaFile(String fileName) {
        this.fileName = fileName;
    }

    public MediaFile() {
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
