package be.kdg.infra3.configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MyStorageService {


    public List<URL> getImageUrls(String bucketName) throws IOException {
        List<URL> imageUrls = new ArrayList<>();

        // Load the service account key file from resources directory
        try (InputStream inputStream = MyStorageService.class.getClassLoader().getResourceAsStream("key.json")) {

            // Check if the input stream is null (file not found)
            if (inputStream == null) {
                throw new FileNotFoundException("key.json not found in resources directory");
            }

            // Load the service account key file
            GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);

            // Authenticate using the service account credentials
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

            // List all blobs in the bucket
            for (Blob blob : storage.list(bucketName).iterateAll()) {
                // Generate a signed URL valid for 10 minutes
                URL signedUrl = storage.signUrl(BlobInfo.newBuilder(bucketName, blob.getName()).build(), 600, TimeUnit.SECONDS);
                imageUrls.add(signedUrl);
            }
        }
        return imageUrls;
    }

    public String uploadFileToBucket(String bucketName, String filename, byte[] fileBytes) throws IOException {
        // Load the service account key file
        GoogleCredentials credentials = GoogleCredentials.fromStream(getClass().getClassLoader().getResourceAsStream("key.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

        // Specify the blob name within the bucket
        BlobId blobId = BlobId.of(bucketName, filename);

        // Define the blob metadata, such as content type
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
                .setContentType("image/jpeg")  // Adjust content type based on your file type
                .build();

        // Upload the file to Google Cloud Storage
        storage.create(blobInfo, fileBytes);

        // Generate and return the URL of the uploaded file
        return String.format("https://storage.googleapis.com/%s/%s", bucketName, filename);
    }


    public void deleteFileFromBucket(String bucketName, String imageUrl) throws IOException {
        // Load the service account key file
        GoogleCredentials credentials = GoogleCredentials.fromStream(getClass().getClassLoader().getResourceAsStream("key.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        // Extract the filename or unique identifier from the imageUrl
        // For example, if the imageUrl is "https://storage.googleapis.com/bucketName/imageName.jpg",
        // you can extract "imageName.jpg" as the filename
        String filename = extractFilenameFromUrl(imageUrl);

        // Create a BlobId for the file to be deleted
        BlobId blobId = BlobId.of(bucketName, filename);

        // Delete the file from the bucket
        boolean deleted = storage.delete(blobId);

        if (deleted) {
            System.out.println("File deleted successfully");
        } else {
            throw new IOException("Failed to delete file from bucket");
        }
    }

private String extractFilenameFromUrl(String imageUrl) {
    // Split the imageUrl by "?"
    String[] parts = imageUrl.split("\\?");
    // The first part contains the filename
    String filenamePart = parts[0];
    // Split the filename part by "/"
    String[] filenameParts = filenamePart.split("/");
    // The last part contains the filename
    return filenameParts[filenameParts.length - 1];
}

}
//
//    public void validateCredentials() throws IOException {
//
//        // Load the service account key file from resources directory
//        InputStream inputStream = MyStorageService.class.getClassLoader().getResourceAsStream("key.json");
//
//        // Check if the input stream is null (file not found)
//        if (inputStream == null) {
//            throw new FileNotFoundException("key.json not found in resources directory");
//        }
//
//        iterateBuckets(inputStream);
//
//    }
//
//    public void iterateBuckets(InputStream inputStream) throws IOException {
//        // Load the service account key file
//        GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);
//        // Authenticate using the service account credentials
//        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
//
//
//        // List buckets to test authentication
//        for (Bucket bucket : storage.list().iterateAll()) {
//            System.out.println(bucket.getName());
//
//            getFileLinks(storage, bucket.getName());
//        }
//    }
//
//    public void getFileLinks(Storage storage, String BucketName) {
//        // List all blobs in the bucket
//        Iterable<Blob> blobs = storage.list(BucketName).iterateAll();
//
//        // Generate signed URLs for each blob and print them
//        for (Blob blob : blobs) {
//            // Generate a signed URL valid for 10 minutes
//            URL signedUrl = storage.signUrl(BlobInfo.newBuilder(BucketName, blob.getName()).build(), 600000, TimeUnit.MILLISECONDS);
//
//            // Print the signed URL
//            System.out.println("Signed URL for " + blob.getName() + ": " + signedUrl);
//        }
//    }
