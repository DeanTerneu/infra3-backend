package be.kdg.infra3.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class config implements WebMvcConfigurer {


    @Bean
    public MyStorageService myStorageService() {return new MyStorageService();}
}
