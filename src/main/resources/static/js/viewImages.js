//
// const previewButton = document.getElementById('preview-btn')
// previewButton.addEventListener('click', async function() {
//     try {
//         const response = await fetch('/api/mediafiles');
//         const data = await response.json();
//
//         const imageContainer = document.getElementById('imageContainer');
//         imageContainer.innerHTML = '';
//
//         data.forEach(imageUrl => {
//             const img = document.createElement('img');
//             img.src = imageUrl;
//             imageContainer.appendChild(img);
//         });
//
//     } catch (error) {
//         console.error('Error fetching image URLs:', error);
//     }
// });

const previewButton = document.getElementById('preview-btn');

previewButton.addEventListener('click', async function() {
    try {
        const response = await fetch('/api/mediafiles');
        const data = await response.json();

        const imageContainer = document.getElementById('imageContainer');
        imageContainer.innerHTML = '';

        data.forEach(imageUrl => {
            const imgContainer = document.createElement('div');

            const img = document.createElement('img');
            img.src = imageUrl;
            img.style.maxWidth = '50%';

            const deleteBtn = document.createElement('button');
            deleteBtn.textContent = 'Delete';
            deleteBtn.addEventListener('click', async function() {
                try {
                    const deleteResponse = await fetch('/api/mediafiles/delete', {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({ imageUrl })
                    });
                    if (deleteResponse.ok) {
                        console.log('Image deleted successfully');
                        imgContainer.remove();
                    } else {
                        throw new Error('Failed to delete image');
                    }
                } catch (error) {
                    console.error('Error deleting image:', error);
                }
            });

            imgContainer.appendChild(img);
            imgContainer.appendChild(deleteBtn);
            imageContainer.appendChild(imgContainer);
        });

    } catch (error) {
        console.error('Error fetching image URLs:', error);
    }
});
